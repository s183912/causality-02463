import os, sys
from glob import glob as glob
os.chdir(os.path.join(sys.path[0], ".."))

import numpy as np

def load(*interventions, test=True) -> np.ndarray:
	"""
	Henter data fra data/*.csv. Hvis test==True, hentes fra test-data, ellers actual-data
	interventions kan specificeres med fx 'Q=0'
	Hvis man fx vil intervenere med Q=1 og S=0 kaldes load('Q=1', 'S=0')
	Hvis en fil med ønskede data ikke findes, laver funktionen en FileNotFoundError
	Shape: (4 hvis test ellers 6) x 100
	Rækker hvis test: Q0, R1, S2, T3
	Rækker ellers: I0, P1, S2, B3, K4, A5
	"""
	if interventions == ([],):
		interventions = []
	interventions = sorted(interventions)
	datadir = "test-data" if test else "real-data"
	csvs = glob(f"{datadir}/*.csv")
	for csv in csvs:
		i = csv.split("/")[-1][:-4]  # Fjern mappe og .csv
		i = i.split("_")[1:]  # Fjen data-x og indel i interventioner
		if sorted(i) == interventions:
			break
	else:
		raise FileNotFoundError(f"Der blev ikke fundet en datafil svarende til interventionerne {interventions}")

	data = np.genfromtxt(csv, skip_header=1, delimiter=",")
	return data[:, 1:].T


if __name__ == "__main__":
	# Tests
	data_all = load()
	assert data_all.shape == (4, 1000)

	data_Q0 = load("Q=0")
	assert data_Q0.shape == (4, 1000)
	assert np.all(data_Q0[0] == 0)

	data_Q0_R0 = load("Q=0", "R=0")
	assert data_Q0_R0.shape == (4, 100)
	assert np.all(data_Q0_R0[[0, 1]] == 0)

	data_Q0_R0 = load("R=0", "Q=0")
	assert data_Q0_R0.shape == (4, 100)
	assert np.all(data_Q0_R0[[0, 1]] == 0)

	try:
		load("F=69")
		assert False
	except FileNotFoundError:
		pass

