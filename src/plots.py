import numpy as np
from dataload import *
import seaborn as sns
import pandas as pd
import pandas.plotting
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

data = load()

Q = data[0,:]
R = data[1,:]
S = data[2,:]
T = data[3,:]
x = Q
y = R

## Plot of Q against R
fig = plt.figure()

x = Q
y = R
gs = GridSpec(4,4)

ax_joint = fig.add_subplot(gs[1:4,0:3])
ax_marg_x = fig.add_subplot(gs[0,0:3])
ax_marg_y = fig.add_subplot(gs[1:4,3])

ax_joint.scatter(x,y)
ax_marg_x.hist(x)
ax_marg_y.hist(y,orientation="horizontal")

# Turn off tick labels on marginals
plt.setp(ax_marg_x.get_xticklabels(), visible=False)
plt.setp(ax_marg_y.get_yticklabels(), visible=False)

# Set labels on joint
ax_joint.set_xlabel('First node')
ax_joint.set_ylabel('Second node')

# Set labels on marginals
ax_marg_y.set_xlabel('Distribution of second node')
ax_marg_x.set_ylabel('Distribution of first node')
plt.show()

# Subplots with variables plotted against each other

df = pd.DataFrame({'Q':Q,
                   'R':R,
                   'S':S,
				   'T':T})

print(df)
pd.plotting.scatter_matrix(df)
