import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib.gridspec import GridSpecFromSubplotSpec
from matplotlib.patches import Ellipse
plt.rcParams.update({"font.size": 14})
import numpy as np
import os

from dataload import load
from logger import Logger

test: bool
labs: str
log: Logger

def causal(is_test=True, plots=True):
	global test, labs, log
	log = Logger(f"src/local_{'test' if is_test else 'real'}.log", f"{'Test' if is_test else 'Real'} case")
	test = is_test
	labs = list("QRST" if test else "IPSBKA")
	corr = corr_and_plots(plots=plots)
	[corr_and_plots(f"{var}=0", plots=plots) for var in labs]
	if test:
		corr_and_plots("Q=0", "S=0", plots=plots)
	else:
		corr_and_plots("I=2", plots=plots)
		corr_and_plots("I=0", "K=0", plots=plots)
		corr_and_plots("I=0", "B=0", plots=plots)
		corr_and_plots("B=0", "P=0", plots=plots)
		corr_and_plots("B=0", "K=0", plots=plots)
	for var1 in range(len(labs)):
		for var2 in range(var1+1, len(labs)):
			qs = list(), [f"{labs[var1]}=0"], [f"{labs[var2]}=0"]
			datas = [load(*q, test=test) for q in qs]
			log.section(f"{labs[var1]}, {labs[var2]}")
			log("\n".join([
				f"Correlation:     {corr[var1][var2]:.2f}",
				f"No intervention: mu_{labs[var1]} = {np.mean(datas[0][var1]):.2f} and mu_{labs[var2]} = {np.mean(datas[0][var2]):.2f}",
				f"sigma_mu_{labs[var1]} = {np.std(datas[0][var1])/np.sqrt(len(datas[0][var1])):.3f}, sigma_mu_{labs[var2]} = {np.std(datas[0][var2])/np.sqrt(len(datas[0][var2])):.3f}",
				f"{labs[var1]} = 0: mu_{labs[var2]} = {np.mean(datas[1][var2]):.2f}, sigma_mu = {np.std(datas[1][var2])/np.sqrt(len(datas[1][var2])):.3f}",
				f"{labs[var2]} = 0: mu_{labs[var1]} = {np.mean(datas[2][var1]):.2f}, sigma_mu = {np.std(datas[2][var1])/np.sqrt(len(datas[2][var1])):.3f}",
			]))

	log.section("Correlation table")
	lines = ["\\hline\n & " + " & ".join([f"${lab}$" for lab in labs]) + r"\\\hline"]
	for i, lab in enumerate(labs):
		line = [f"${lab}$"]
		for j, inner_lab in enumerate(labs):
			line.append(f"${corr[i][j]:.3f}$")
		lines.append(" & ".join(line) + r"\\\hline")
	tab = "\n".join(lines)
	log(tab)

	log.section("Latex table")
	lines = ["\\hline\n & No intervention & " + " & ".join(f"${var}=0$" for var in labs) + r"\\\hline"]
	datas = [load(q, test=test) for q in [list(), *[f"{var}=0" for var in labs]]]
	for i, lab in enumerate(labs):
		line = f"${lab}$"
		for j, dat in enumerate(datas):
			line += f" & ${dat[i].mean():.2f}$, ${dat[i].std():.2f}$, ${dat[i].std()/np.sqrt(len(dat[i])):.2f}$"
		line += r"\\\hline"
		lines.append(line)
	tab = "\n".join(lines)
	log(tab)

def marginal_plots(data, dir):
	plt.figure(figsize=(15, 10))
	for i, lab in enumerate(labs):
		plt.subplot(*([1, 4] if test else [2, 3]), i+1)
		plt.hist(data[i], density=True)
		plt.xlabel(lab)
		plt.ylabel("Frequency")
	dir_path = f"src/local_plots_{'t' if test else 'r'}/{dir}"
	plt.savefig(f"{dir_path}/marginals.png")
	plt.clf()

def corr_and_plots(*interventions, plots=True):
	title = "No intervention" if not interventions else ", ".join(interventions)
	data = load(*interventions, test=test)
	corr = np.corrcoef(data)
	log.section(title)
	log(f"{', '.join(labs)}\n{corr}")
	if plots:
		marginal_plots(data, title)
		plot(data, corr, title)
	for var in range(len(data)):
		log(f"mu_{labs[var]} = {np.mean(data[var]):.2f}, sigma_{labs[var]} = {np.std(data[var]):.3f}, sigma_mu = {np.std(data[var])/np.sqrt(len(data[var])):.3f}")
	return corr

def plot(data: np.ndarray, corr: np.ndarray, title: str):
	assert data.shape[0] == 4 if test else 6
	# Big plots
	plt.figure(figsize=(19.2, 10.8))
	plots = []
	for i in range(len(labs)):
		for j in range(i+1, len(labs)):
			plots.append((labs[i], labs[j]))
	for i in range(len(plots)):
		var1, var2 = labs.index(plots[i][0]), labs.index(plots[i][1])
		plt.subplot(2, 3, i+1) if test else plt.subplot(3, 5, i+1)
		plt.scatter(data[var1], data[var2])
		plt.xlabel(labs[var1])
		plt.ylabel(labs[var2])
		plt.title(f"Correlation: {corr[var1][var2]:.2f}")
		plt.grid(True)
	plt.tight_layout()

	dir_path = f"src/local_plots_{'t' if test else 'r'}"
	os.makedirs(dir_path, exist_ok=True)
	plt.savefig(f"{dir_path}/{title}.png")
	plt.clf()

	# Jointplot subplots
	plt.figure(figsize=(19.2, 10.8))
	for i in range(len(plots)):
		var1, var2 = labs.index(plots[i][0]), labs.index(plots[i][1])
		f = sns.jointplot(data[var1], data[var2], ratio=2)
		f.set_axis_labels(labs[var1], labs[var2])
		if len(title) == 3 and title[1] == "=":
			othervar = var1 if labs[var2] in title else var2
			plt.title(f"{title}, mu_{labs[othervar]}={np.mean(data[othervar]):.2f}\nCorrelation: {corr[var1][var2]:.2f}")
		else:
			plt.title(f"{title}\nmu_{labs[var1]}={np.mean(data[var1]):.2f}, mu_{labs[var2]}={np.mean(data[var2]):.2f}\nCorrelation: {corr[var1][var2]:.2f}")
		plt.grid(True)
		plt.tight_layout()
		
		os.makedirs(f"{dir_path}/{title}", exist_ok=True)
		f.savefig(f"{dir_path}/{title}/{title}, {labs[var1]}, {labs[var2]}.png")
		plt.clf()

if __name__ == "__main__":
	causal(True)
	causal(False)

